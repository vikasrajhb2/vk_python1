def cal(action,a,b):
    if(action=="add"):
        return (a+b)
    if(action=="subtract"):
        return (a-b)
    if(action=="multiply"):
        return (a*b)
    if(action=="divide"):
        return (a/b)
    if(action=="modulus"):
        return (a%b)    

if __name__ == "__main__":
    print("Enter the action")
    action = input()
    print("Enter two numbers a and b")
    a = int(input())
    b = int(input())
    res = cal(action,a,b)
    print("Result is "+str(res))